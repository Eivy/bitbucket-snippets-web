package main

type templateData struct {
	RawURL     string
	SnippetURL string
	Title      string
	Files      map[string]string
}
