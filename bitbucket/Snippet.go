package bitbucket

import (
	"errors"
	"log"
	"net/url"
	"time"

	jsonpointer "github.com/mattn/go-jsonpointer"
)

// Snippet is Data from bitbucket API
type Snippet struct {
	User    string
	ID      string
	Type    string
	Title   string
	URL     *url.URL
	Files   map[string]*url.URL
	GetTime time.Time
}

// GetData returns Snippet from bitbucket API
func GetData(user, id string) (data Snippet, err error) {
	once.Do(initial)
	if v, b := cache[user+"/"+id]; b {
		data = v
		return
	}
	log.Println("Get data for " + user + "/" + id)
	v, err := getJSON(user, id)
	if err != nil {
		return
	}
	data.User = user
	data.ID = id
	data.GetTime = time.Now()
	t, err := jsonpointer.Get(v, "/links/html/href")
	if err != nil {
		return
	}
	u, err := url.Parse(t.(string))
	if err != nil {
		return
	}
	data.URL = u
	t, err = jsonpointer.Get(v, "/type")
	if err != nil {
		return
	}
	if t != "snippet" {
		err = errors.New("Specified ID is not for snippet")
		return
	}
	data.Type = t.(string)
	t, err = jsonpointer.Get(v, "/title")
	data.Title = t.(string)
	files, err := jsonpointer.Get(v, "/files")
	data.Files = make(map[string]*url.URL)
	for name, file := range files.(map[string]interface{}) {
		var href interface{}
		href, err = jsonpointer.Get(file, "/links/self/href")
		if err != nil {
			return
		}
		var u *url.URL
		u, err = url.Parse(href.(string))
		if err != nil {
			return
		}
		data.Files[name] = u
	}
	if _, b := data.Files["index.html"]; !b {
		err = errors.New("No index.html found")
		return
	}
	cache[user+"/"+id] = data
	return
}
