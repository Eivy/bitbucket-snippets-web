package bitbucket

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"net/http"
	"path"
	"sync"
	"time"

	jsonpointer "github.com/mattn/go-jsonpointer"
)

var cache map[string]Snippet
var userCache map[string][]Snippet
var ticker *time.Ticker
var once sync.Once

// GetDataUsersAll returs all user's snippet data
func GetDataUsersAll(user string) (data []Snippet, err error) {
	once.Do(initial)
	if v, b := userCache[user]; b {
		data = v
		return
	}
	v, err := getJSON(user)
	if err != nil {
		return
	}
	wg := &sync.WaitGroup{}
	files, err := jsonpointer.Get(v, "/values")
	for _, obj := range files.([]interface{}) {
		wg.Add(1)
		go func(obj interface{}) {
			defer wg.Done()
			id, err := jsonpointer.Get(obj, "/id")
			if err != nil {
				return
			}
			s, err := GetData(user, id.(string))
			if err == nil {
				data = append(data, s)
			}
		}(obj)
	}
	wg.Wait()
	return
}

func initial() {
	cache = make(map[string]Snippet)
	ticker = time.NewTicker(30 * time.Second)
	go func() {
		defer ticker.Stop()
		for {
			select {
			case <-ticker.C:
				for k, v := range cache {
					if time.Now().After(v.GetTime.Add(time.Minute)) {
						log.Println("Cache Delete " + k)
						delete(cache, k)
					}
				}
			}
		}
	}()
}

func getJSON(data ...string) (j interface{}, err error) {
	resJSON, err := http.Get(fmt.Sprintf("https://api.bitbucket.org/2.0/snippets/%v", path.Join(data...)))
	if err != nil {
		return
	}
	defer resJSON.Body.Close()
	b, err := ioutil.ReadAll(resJSON.Body)
	if err != nil {
		return
	}
	err = json.Unmarshal(b, &j)
	if err != nil {
		return
	}
	return
}
