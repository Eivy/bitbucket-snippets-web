# bitbucket-snippets-web

bitbucket-snippets-web publish BitBucket snippets as Website

This working on [Heroku](https://bitbucket-snippets-web.herokuapp.com).

You can see snippets as Website with url like `https://bitbucket-snippets-web.herokuapp.com/$USER/raw/$ID`

Now, raw only.

[This is test page.](https://bitbucket-snippets-web.herokuapp.com/Eivy/raw/a4dReK)

## Inspired by bl.ock.org

[bl.ocks.org](https://bl.ocks.org) shows Gist as Website.

I want to do it with Bitbucket snippets.
