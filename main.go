package main

import (
	"fmt"
	"html/template"
	"io/ioutil"
	"log"
	"mime"
	"net/http"
	"net/url"
	"os"
	"path/filepath"
	"strings"
	"sync"

	"bitbucket.org/Eivy/bitbucket-snippets-web/bitbucket"
)

func main() {
	http.HandleFunc("/", handler)
	err := http.ListenAndServe(":"+os.Getenv("PORT"), nil)
	if err != nil {
		log.Fatal(err)
	}
}

func handler(w http.ResponseWriter, r *http.Request) {
	log.Println("Accessed to " + r.URL.String())
	if strings.Contains(r.URL.Path, "/raw/") {
		raw(w, r)
	} else if !strings.Contains(strings.Trim(r.URL.Path, "/"), "/") {
		user(w, r)
	} else {
		root(w, r)
	}
}

func user(w http.ResponseWriter, r *http.Request) {
	if !strings.HasSuffix(r.URL.Path, "/") {
		http.Redirect(w, r, r.URL.String()+"/", 301)
		return
	}
	user := strings.Trim(r.URL.Path, "/")
	data, err := bitbucket.GetDataUsersAll(user)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	tmplValue, err := Asset("html/user.html")
	tmpl := template.Must(template.New("").Parse(string(tmplValue)))
	if err = tmpl.ExecuteTemplate(w, "", struct {
		Name     string
		Snippets []bitbucket.Snippet
	}{Name: user, Snippets: data}); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func root(w http.ResponseWriter, r *http.Request) {
	user, id := splitURI(r.URL.Path)
	if user == "" || id == "" {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}
	data, err := bitbucket.GetData(user, id)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	var tmplData templateData
	tmplData.Files = make(map[string]string)
	tmplData.RawURL = "/" + user + "/raw/" + id + "/"
	tmplData.SnippetURL = data.URL.String()
	tmplData.Title = data.Title
	wg := &sync.WaitGroup{}
	for n, u := range data.Files {
		fmt.Println(n)
		wg.Add(1)
		go func(n string, u *url.URL) {
			defer wg.Done()
			res, err := http.Get(u.String())
			defer r.Body.Close()
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			b, err := ioutil.ReadAll(res.Body)
			if err != nil {
				http.Error(w, err.Error(), http.StatusInternalServerError)
				return
			}
			tmplData.Files[n] = string(b)
		}(n, u)
	}
	wg.Wait()
	tmplValue, err := Asset("html/snippet.html")
	tmpl := template.Must(template.New("").Parse(string(tmplValue)))
	if err = tmpl.ExecuteTemplate(w, "", tmplData); err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
}

func raw(w http.ResponseWriter, r *http.Request) {
	if len(strings.Split(r.URL.Path, "/")) == 4 && !strings.HasSuffix(r.URL.Path, "/") {
		http.Redirect(w, r, r.URL.String()+"/", 301)
		return
	}
	user, id, filename := splitURIRaw(r.URL.Path)
	log.Println(filename)
	data, err := bitbucket.GetData(user, id)
	res, err := http.Get(fmt.Sprint(data.Files[filename]))
	defer r.Body.Close()
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	b, err := ioutil.ReadAll(res.Body)
	if err != nil {
		http.Error(w, err.Error(), http.StatusInternalServerError)
		return
	}
	ext := filepath.Ext(filename)
	w.Header().Set("Content-Type", mime.TypeByExtension(ext))
	w.Write(b)
}

func splitURI(URI string) (user, id string) {
	uri := strings.Split(URI, "/")
	if len(uri) == 4 || len(uri) == 3 {
		user = uri[1]
		id = uri[2]
	}
	return
}

func splitURIRaw(URI string) (user, id, file string) {
	uri := strings.Split(URI, "/")
	if len(uri) == 5 {
		user = uri[1]
		id = uri[3]
	}
	if len(uri) > 4 && strings.Join(uri[4:], "") != "" {
		file = strings.Join(uri[4:], "/")
	} else {
		file = "index.html"
	}
	return
}
